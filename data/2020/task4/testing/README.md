## CLEF 2020 - CheckThat! Lab 
**Arabic**

**Task:** Task 4 (Claim Verification)

**Dataset:** CT20-AR-Test-T4

**Release Date:** June 1st, 2020


Filenames: 	
==========
- CT20-AR-T4.zip
- CT20-AR-Test-Topics.json
- CT20-AR-Test-T4-Tweets.gz


Dataset Description:
====================
The set contains 201 check-worthy claims, 12 topics and potentially related web pages for these topics for Arabic Task 4.
In addition, we include: the full JSON object of these tweets and html of the Web pages. 


Statistics:
===========
- 201 check-worthy claims (same as those in Task 3)
- 12 Testing topics (same as those in Tasks 1 and 3)


Files Description:
==================
Below is the description of each file in the dataset.

- "CT20-AR-Test-Topics.json" a JSON file containing main information on the topics 
Each line contains a JSON object that corresponds to one topic in the following format:
{
  "topicID": String,
  "title": String,
  "description": String
}
- "CT20-AR-Test-T4-Tweets.gz" contains the JSON object for each check-worthy claim encoded in UTF-8.
Each line contains a JSON object representing one tweet as formatted by Twitter.
The object also includes two extra fields: 
  - topicID: The ID of the topic the tweet belongs to
  - crawlingTime: Time at which the tweet was collected from Twitter
- "CT20-AR-T4.zip" This directory contains one sub-directory per topic. In each sub-directorty, following files/folders are included:
  - "topicID-Test-T4-html-pages" html format of Web pages potentially relevant to the topic
  - "topicID-Test-T4-empty-pages.txt" IDs of Web pages that we couldn't parse or retrieve html format for.
  - "topicID-Test-T4-html-to-url.txt" Tab-seperated file listing urls of the Web pages