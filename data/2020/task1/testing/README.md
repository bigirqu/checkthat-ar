## CLEF 2020 - CheckThat! Lab 

**Arabic Tasks**

**Task**: Task1 (Claim check-worthiness)


**Dataset**: CT20-AR-Test-T1

**Release Date**: May 27th, 2020


## Filenames: 	

- CT20-AR-Test-Topics.json
- CT20-AR-Test-T1-Tweets.gz
- CT20-AR-Test-T1-Labels.txt



## Dataset Description:
The set contains 12 topics and 6K potentially related tweets and their associated labels for check-worthiness identification over Arabic tweets (Arabic Task 1).
In addition, we include: the full json object of these tweets. 


## Statistics:
12 Testing topics, each has 500 tweets. The dataset includes 1604 check-worthy tweets out of all 6,000 tweets.


Below is the description of each file in the dataset.

- "CT20-AR-Test-Topics.json" a JSON file containing main information on the topics 
Each line contains a JSON object that corresponds to one topic in the following format:
{
  "topicID": String,
  "title": String,
  "description": String
}

- "CT20-AR-Test-T1-Tweets.gz" contains the json object for each tweet encoded in UTF-8.
Each line contains a JSON object representing one tweet as formatted by Twitter.
The object also includes two extra fields: 
topicID: The ID of the topic the tweet belongs to
crawlingTime: Time at which the tweet was collected from Twitter

- "CT20-AR-Test-T1-Labels.txt" contains check-worthiness annotations for the tweets. 
Each line is tab-seperated with the format: "topicID	tweetID	label" 
where "label" values can be: [1: check-worthy, 0: not check-worthy]
